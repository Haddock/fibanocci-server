FROM python:3

RUN adduser fibanocci
WORKDIR /home/fibanocci

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

COPY app app
COPY fibanocci.py boot.sh ./

RUN chmod +x boot.sh

ENV FLASK_APP fibanocci.py

RUN chown -R fibanocci:fibanocci ./
USER fibanocci

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
