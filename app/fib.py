class FibanocciFac():
    def __init__(self):
        self.cache = [1, 1]
        self.largest_fib = 1

    def fibanocci(self, start, end):
        if end > self.largest_fib:
            self.calc_fib(end)
        return sum(self.cache[start - 1:end])
    
    def calc_fib(self, end):
        a = self.cache[self.largest_fib - 1] + self.cache[self.largest_fib]
        b = self.cache[self.largest_fib] + a
        n = self.largest_fib
        while n < end - 1:
            self.cache.append(a)
            a, b = b, a + b
            n = n + 1
        self.largest_fib = end - 1

        

