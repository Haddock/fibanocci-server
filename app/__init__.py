from flask import Flask
import logging
from app import logger

app = Flask(__name__)

from app import routes

logging.basicConfig(
    format='%(asctime)s %(levelname)s %(message)s', 
    level=logging.INFO, 
    datefmt='%m/%d/%Y %I:%M:%S %p'
    )

root = logging.getLogger()
dbhandler = logger.LogDBHandler()
root.addHandler(dbhandler)