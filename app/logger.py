from logging import StreamHandler, Formatter
import sqlite3
import os
import re

class LogDBHandler(StreamHandler):

    def __init__(self,):
        StreamHandler.__init__(self)

        formatter = Formatter('%(message)s')
        self.setFormatter(formatter)

        self.db_file = os.getcwd() + "/fibrequests.db"
        if not os.path.isfile(self.db_file):
            conn = sqlite3.connect(self.db_file)
            c = conn.cursor()
            c.execute('''CREATE TABLE response(
                        response_id INTEGER PRIMARY KEY,
                        status_code INTEGER, 
                        data TEXT)''')
            c.execute('''CREATE TABLE request
                        (request_id INTEGER PRIMARY KEY,
                        date TEXT, 
                        ip TEXT, 
                        url_method TEXT,
                        assoc_response INTEGER,
                        FOREIGN KEY(assoc_response) REFERENCES response(response_id))''')
            conn.commit()
            conn.close()
            
    def emit(self, record):
        msg = self.format(record)
        if "GET /fib" in msg:
            conn = sqlite3.connect(self.db_file)
            c = conn.cursor()

            ip = msg.split(' ')[0]
            date = msg[msg.find('[') + 1:msg.find(']')]
            first = msg.find('\"') + 1
            srequest = msg[first:msg.find('\"',first)]
            
            c.execute('''INSERT INTO request (date, ip, url_method, assoc_response) 
                        VALUES (?, ?, ?, (SELECT MAX(response_id) FROM response))''', (date, ip, srequest))
            conn.commit()
            conn.close()
        elif "Response " in msg:
            conn = sqlite3.connect(self.db_file)
            c = conn.cursor()

            status_code = msg.split(' - ')[1]
            data = msg.split(' - ')[2]
            c.execute('''INSERT INTO response (status_code, data) 
                        VALUES (?, ?)''', (status_code, data))
            conn.commit()
            conn.close()
