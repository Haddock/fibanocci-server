from app import app, fib
from flask import abort, request

fac = fib.FibanocciFac()

@app.route('/health')
def get_health():
    tests = []
    tests.append(fac.fibanocci(3, 5) == 10)
    tests.append(fac.fibanocci(5, 20) == 17703)

    if False not in tests:
        return "healthy"
    else:
        return "sick, ill, unhealthy"

@app.route('/fib/<int:start_idx>/<int:end_idx>', methods=['GET'])
def get_fibanocci(start_idx, end_idx):
    if start_idx < 1 or end_idx < 1 or start_idx > end_idx:
        abort(400)
    answer = fac.fibanocci(start_idx, end_idx)
    return "{}".format(answer)

@app.after_request
def after(response):
    app.logger.info("Response - {} - {}".format(response.status_code, response.get_data()))
    return response